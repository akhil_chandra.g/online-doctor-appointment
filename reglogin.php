<?php include('reglog.php') ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Login</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<style media="screen">
.card
{
  margin-top:80px;
  border:4px solid #ff0000;
}
  .btn{
    background-color: #1089ff;
    color:#fff;
  }
  img{
    margin-bottom: 10px;
  }
</style>
  </head>
  <body>
    <div class="container">

    <div class="row justify-content-center">
      <div class="col-lg-5 col-md-4">
        <div class="card shadow p-3 mb-5 bg-white rounded" >
          <div class="card-body">
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12">
                <img src="images.png" height="205px"width="437px"style="margin-left:-36px; margin-top:-36px;">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Login</h1>
                  </div>
                  <form action="" method="post">
                    <div class="form-group">
                      <input type="text" class="form-control "  name="username" placeholder="Enter Username">
                    </div>
                    <div class="form-group">
                      <input type="password" class="form-control " name="password" placeholder="Password">
                    </div>
                    <div class="form-group text-center">
          <button type="submit" name="login" class="btn">LOGIN</button>
          </div>
                  </form>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>
  </body>
</html>
