<?php include('log.php') ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Login</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<style media="screen">
body{
  background-image: linear-gradient(to right top, #172f53, #2e4773, #456195, #5c7cb8, #7598dc);
  background-repeat: no-repeat;
  height: 100vh;
}
.card
{
  margin-top:20px;
  border:8px solid #2a1a5e;
}
  .btn{
    background-color: #1089ff;
    color:#fff;
  }
</style>
  </head>
  <body>
    <div class="container">

    <div class="row justify-content-center">
      <div class="col-lg-5 col-md-4">
        <div class="card shadow p-3 mb-5 bg-white rounded" >
          <div class="card-body">
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12">
                  <div class="text-center">
                  <img src="495078-PHE69C-919.jpg" height="230px" style="border-radius:25%;margin-bottom:20px;">
                    <h1 class="h4 text-gray-900 mb-4">Login</h1>
                  </div>
                  <form action="" method="post">
                    <div class="form-group">
                      <input type="text" class="form-control "  name="username" placeholder="Enter Username">
                    </div>
                    <div class="form-group">
                      <input type="password" class="form-control " name="password" placeholder="Password">
                    </div>
                    <div class="form-group text-center">
          <button type="submit" name="login" class="btn">LOGIN</button>
          <hr>
          </div>
          <div class="text-center">
            <a href="register.php">New Account?Register!!</a>
          </div>
                  </form>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>
  </body>
</html>
