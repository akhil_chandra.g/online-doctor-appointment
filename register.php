<!DOCTYPE html>
<html lang="en">

<head>

  <title>Register</title>

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</head>

<body class="bg-gradient-primary" style="background-color: #d3f6f3;">

  <div class="container">
    <div class="row justify-content-center">

    <div class="card col-lg-6 ">
    <div class="card " style="border-radius:25px;">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-12">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Create an Account</h1>
              </div>
              <form action="process1.php" method="post">
                <div class="form-group">
                    <label>Name:</label>
                    <input type="text" class="form-control form-control-user"name="name" placeholder="Name">
                </div>
                <div class="form-group">
                    <label>PhoneNumber:</label>
                    <input type="text" class="form-control form-control-user" name="phoneno"  placeholder="Phonenumber">
                </div>
                <div class="form-group">
                  <label>Gender:</label>
                  <select class="custom-select custom-select-md" name="gender">
                    <option>Male</option>
                    <option>Female</option>
                    <option>Others</option>
                  </select>
                </div>
                <div class="form-group">
                  <label>Email</label>
                  <input type="email" class="form-control form-control-user" name="email" placeholder="Enter Email">
                </div>
                <div class="form-group">
                  <textarea name="address" class="form-control" rows="8" cols="10"></textarea>
                </div>
                <div class="form-group">
                    <label>Age</label>
                    <input type="text" class="form-control form-control-user" name="age" placeholder="Age">
                  </div>
                    <div class="form-group">
                      <label>username</label>
                    <input type="text" class="form-control form-control-user" name="username" placeholder="Username" >
                  </div>
                <div class="form-group">
                    <label>Password:</label>
                    <input type="password" class="form-control form-control-user" name="password" placeholder="Password">
                  </div>
                  <div class="col-12">
          <label class="btn ">
              Browser Image   <input type="file" class="btn" name="pictures" style="display:none">
          </label>

        </div>
                <button class="btn btn-primary btn-user btn-block" name="reg" type="submit">Register Account</button>

                </a>
              </form>
              <hr>
              <div class="text-center">
                <a class="small" href="patient.html">Already have an account? Login!</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>


</body>

</html>
