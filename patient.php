<?php
	include('log.php');
?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Home</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<style media="screen">
body {
background-image: linear-gradient(to right top, #172f53, #2e4773, #456195, #5c7cb8, #7598dc);
background-repeat: no-repeat;
height: 100vh;
background-repeat: no-repeat;
color: #f7f7f7;
font-family: 'Lato', sans-serif;
font-weight: 300;
}



.btn {
flex: 1 1 auto;
margin: 10px;
padding: 20px;
border: 2px solid #f7f7f7;
text-align: center;
text-transform: uppercase;
position: relative;
overflow: hidden;
transition: .3s;
color:#f1f1f1;
margin-top: 20px;
}


#apply,#login
{
padding:10%;
padding-right: 5%;

}a{
	color:#ffff;
}
a:hover{
	text-decoration: none;
}

</style>

  </head>
  <body>
		<nav class="navbar navbar-expand-lg navbar-dark">
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		<i class="fa fa-bars" aria-hidden="true"></i>
	</button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav navbar-nav mr-auto">
    </ul>
    <ul class="navbar-nav navbar-nav ">
      <li class="nav-item">
				<?php echo '<img src="data:image/jpeg;base64,'.base64_encode($_SESSION['patients']['image'] ).'" height="50" width="50"style="border-radius:50%;margin-right:20px;" />'; ?><a href="login.php?logout='1'">Logout</a>
      </li>
    </ul>
  </div>
</nav>


	    <div class="conatiner p-3">
	      <div class="row animated fadeInUp slow delay-1s">
	        <div class="col-lg-6"id="apply">
	          <div class="row animated fadeInUp slow">
	            <div class="col-12">
	              <img src="" alt="" height="350px" width="400px;">
	            </div>
	            <div class="col-12 text-center">
	              <a href="e-pha.php" class="btn">E-Pharmacy</a>
	            </div>
	          </div>
	        </div>
	        <div class="col-lg-1" >
	        </div>
	        <div class="col-lg-5"id="login">
	          <div class="row">
	            <div class="col-12"  >
	              <img src="" alt="" height="350px" width="400px;">
	            </div>
	            <div class="col-lg-12 text-center">
	              <a href="appointment.php" class="btn">Appointment</a>
	            </div>
	          </div>
	        </div>
	      </div>
	        </div>


  </body>
</html>
