-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 10, 2019 at 05:38 PM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `online_doctor_appointment`
--

-- --------------------------------------------------------

--
-- Table structure for table `appointment`
--

CREATE TABLE `appointment` (
  `doctor_id` int(20) NOT NULL,
  `patient_id` int(20) NOT NULL,
  `date_of_visit` date NOT NULL,
  `session` varchar(20) NOT NULL,
  `description` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `appointment`
--

INSERT INTO `appointment` (`doctor_id`, `patient_id`, `date_of_visit`, `session`, `description`) VALUES
(3, 1, '2019-08-21', 'MORNING', 'CHILD SPECIALIST'),
(5, 2, '2019-08-21', 'AFTERNOON', 'DERMATOLOGIST'),
(1, 2, '2019-08-21', 'MORNING', 'CARDIOLOGIST');

-- --------------------------------------------------------

--
-- Stand-in structure for view `app_view`
-- (See below for the actual view)
--
CREATE TABLE `app_view` (
`PATIENT_ID` int(20)
,`doctor_DESIGNATION` varchar(20)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `days`
-- (See below for the actual view)
--
CREATE TABLE `days` (
`PATIENT_ID` int(20)
,`Patient_NAME` varchar(30)
);

-- --------------------------------------------------------

--
-- Table structure for table `doctor_details`
--

CREATE TABLE `doctor_details` (
  `DOCTOR_ID` int(20) NOT NULL,
  `DOCTOR_NAME` varchar(30) NOT NULL,
  `DOCTOR_PHONE` varchar(10) NOT NULL,
  `DOCTOR_EMAIL` varchar(40) NOT NULL,
  `DOCTOR_PASSWORD` varchar(20) NOT NULL,
  `DOCTOR_ADDRESS` varchar(40) NOT NULL,
  `DOCTOR_DESIGNATION` varchar(20) NOT NULL,
  `DOCTOR_PHOTO` blob DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doctor_details`
--

INSERT INTO `doctor_details` (`DOCTOR_ID`, `DOCTOR_NAME`, `DOCTOR_PHONE`, `DOCTOR_EMAIL`, `DOCTOR_PASSWORD`, `DOCTOR_ADDRESS`, `DOCTOR_DESIGNATION`, `DOCTOR_PHOTO`) VALUES
(1, 'Farhan Ali', '9960822033', ' farhanali@gmail.com', '0000', 'Hyderbad', 'cardiologist', NULL),
(2, 'Mukesh Batra', '7901017134', 'mukesh.batra11@gmail.com', '0000', 'chennai', 'endocrinologist', NULL),
(3, 'Pramod Gandhi', '9121856586', ' pramodgandhi@hotmail.com', '0000', 'mumbai', 'child specialist', NULL),
(4, 'Sonali Patili', '8767234762', ' sonalipatil1@gmail.com', '0000', ' Hyderabad', 'gynaecologist', NULL),
(5, 'Kuldeep Rasal', '9920762366', 'kuldeep12@yahoo.com ', '0000', 'kolkata ', 'dermatologist', NULL),
(6, 'Asha Devi', '9938154525', ' asha.devi@gmail.com', '0000', ' mumbai ', 'psychiatrist', NULL),
(7, 'Kavita Shukla', '8655294933', ' kavita2011@gmail.com ', '0000', 'mumbai ', 'endocrinologist', NULL),
(8, 'Raja Kannam', '9121540488', ' rajakannan@gmail.com ', '0000', 'Bangalore ', 'physician', NULL),
(9, 'Imran Sheik', '7031261527', ' sheikhimran@gmail.com ', '0000', 'delhi ', 'cardiologist ', NULL),
(10, 'Deepika Das', '9966422965', 'deepikadas22@gmail.com ', '0000', 'Hyderabad ', 'gynaecologist ', NULL),
(11, 'Bindu Sharma', '9984877772', ' sharmabindu30@gmail.com ', '0000', 'Chennai ', 'child specialist ', NULL),
(12, 'Kishor kumar', '9619439944', ' kumarkishor@gmail.com', '0000', ' Chennai ', 'psychiatrist ', NULL),
(13, 'Usha Karti', '9833932017', ' ushakhatri@gmail.com ', '0000', 'Delhi ', 'dentistry', NULL),
(14, 'Durai Rajesh', '7665325840', ' durairajesh11@gmail.com', '0000', ' Bangalore ', 'physician', NULL),
(15, 'Komal Dixit', '8886936922', ' komaldixit11@gmail.com', '0000', ' mumbai ', 'dermatologist', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `patients_details`
--

CREATE TABLE `patients_details` (
  `patient_id` int(20) NOT NULL,
  `patient_name` varchar(30) NOT NULL,
  `PATIENT_DOB` date NOT NULL,
  `PATIENT_GENDER` varchar(10) NOT NULL,
  `patient_phone` varchar(10) NOT NULL,
  `patient_email` varchar(40) NOT NULL,
  `PATIENT_PASSWORD` varchar(20) NOT NULL,
  `patient_address` varchar(40) NOT NULL,
  `patient_age` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `patients_details`
--

INSERT INTO `patients_details` (`patient_id`, `patient_name`, `PATIENT_DOB`, `PATIENT_GENDER`, `patient_phone`, `patient_email`, `PATIENT_PASSWORD`, `patient_address`, `patient_age`) VALUES
(1, 'Lahari Vuyyuri ', '1999-08-12', 'FEMALE', '8767234762', ' lahari.vuyyuri@gmail.com', '0000', 'mumbai', 18),
(2, 'Jyothirmaie Dhulipalla', '1999-08-20', 'FEMALE', '7901017132', 'jyothidhulipalla99@gmail.com', '0000', 'Hyderabad', 20),
(3, 'Likhith Nemani ', '1999-12-31', 'MALE', '9121865656', 'likhithnemani1@gmail.com', '0000', 'Chennai', 22),
(4, 'Sangeeta Srinath', '2000-03-13', 'FEMALE', '7023756532', ' sangeesrinath@gmail.com', '0000', 'Bangalore', 19),
(5, 'Pavan krishna', '2000-12-11', 'MALE', '8866554703', ' kpavankrishna@gmail.com', '0000', 'Hyderabad', 16);

-- --------------------------------------------------------

--
-- Structure for view `app_view`
--
DROP TABLE IF EXISTS `app_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `app_view`  AS  select `appointment`.`patient_id` AS `PATIENT_ID`,`doctor_details`.`DOCTOR_DESIGNATION` AS `doctor_DESIGNATION` from (`appointment` join `doctor_details`) where `appointment`.`doctor_id` = `doctor_details`.`DOCTOR_ID` ;

-- --------------------------------------------------------

--
-- Structure for view `days`
--
DROP TABLE IF EXISTS `days`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `days`  AS  select `patients_details`.`patient_id` AS `PATIENT_ID`,`patients_details`.`patient_name` AS `Patient_NAME` from `patients_details` where `patients_details`.`patient_id` in (select `appointment`.`patient_id` from `appointment` where `appointment`.`doctor_id` = 1) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `appointment`
--
ALTER TABLE `appointment`
  ADD KEY `doctor_id` (`doctor_id`),
  ADD KEY `patient_id` (`patient_id`);

--
-- Indexes for table `doctor_details`
--
ALTER TABLE `doctor_details`
  ADD PRIMARY KEY (`DOCTOR_ID`);

--
-- Indexes for table `patients_details`
--
ALTER TABLE `patients_details`
  ADD PRIMARY KEY (`patient_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `doctor_details`
--
ALTER TABLE `doctor_details`
  MODIFY `DOCTOR_ID` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `patients_details`
--
ALTER TABLE `patients_details`
  MODIFY `patient_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `appointment`
--
ALTER TABLE `appointment`
  ADD CONSTRAINT `appointment_ibfk_1` FOREIGN KEY (`doctor_id`) REFERENCES `doctor_details` (`DOCTOR_ID`),
  ADD CONSTRAINT `appointment_ibfk_2` FOREIGN KEY (`patient_id`) REFERENCES `patients_details` (`patient_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
