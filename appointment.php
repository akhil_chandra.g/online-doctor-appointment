<?php
	include('log.php');
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Appointment</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
      <style>
      .card
      {
        margin-top: 100px;
        border-radius: 50px;
      }
      img{
        margin-top: -50px;
        border-radius: 100px;
      }
      form
      {
        padding:20px;
      }
      label
      {
        margin-left: 5px;
      }
      #hospital
      {
        margin-top: 100px;
        margin-left: 250px;
        height: 400px;
        width: 300px;
        border-radius: 25px;
        -webkit-box-shadow: 0px 2px 5px 0px rgba(0, 0, 0, 0.1);
        -moz-box-shadow: 0px 2px 5px 0px rgba(0, 0, 0, 0.1);
        box-shadow: 0px 2px 5px 0px rgba(0, 0, 0, 0.1);
        padding: 40px;
        padding-right: 35px;
      }
      .sidenav {
  height: 100%;
  width: 0;
  position: fixed;
  z-index: 1;
  top: 0;
  left: 0;
  background-color: #2a1a5e;
  overflow-x: hidden;
  transition: 0.5s;
  padding-top: 60px;
}

.sidenav a {
  padding: 13px;
  text-decoration: none;
  font-size: 25px;
  color: #f1f1f1;
  display: block;
  transition: 0.3s;
}



.sidenav .closebtn {
  position: absolute;
  top: 0;
  right: 25px;
  font-size: 36px;
  margin-left: 50px;
}

#main {
  transition: margin-left .5s;
  padding: 16px;
}
li{
  list-style: none;
  color:#f1f1f1;
}

@media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}
  </style>
  </head>
  <body>
    <div id="mySidenav" class="sidenav">
      <ul>
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
        <li><a href="patient.php">Dashboard</a> </li>
        <li><a href="appointment.php">Appointment</a></li>
        <li> <a href="e-pha.php">E-Pharmacy</a></li>
      </ul>
      <ul  style="margin-top:300px;">
        <li><?php echo '<img src="data:image/jpeg;base64,'.base64_encode($_SESSION['patients']['image'] ).'" height="50" width="50"style="border-radius:50%;margin-right:20px;" />'; ?><strong><?php echo $_SESSION['patients']['name']; ?></strong></li>
          <li><a href="login.php?logout='1'">Logout</a></li>
      </ul>
    </div>

    <div id="main">
      <span style="font-size:50px;cursor:pointer" onclick="openNav()">&#8801;</span>
    </div>

    <script>
    function openNav() {
      document.getElementById("mySidenav").style.width = "250px";
      document.getElementById("main").style.marginLeft = "250px";
    }

    function closeNav() {
      document.getElementById("mySidenav").style.width = "0";
      document.getElementById("main").style.marginLeft= "0";
    }
    </script>

    <section class="section-padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
            <div class="card shadow p-3 mb-5 ">
                <div class="appointment-form  mt-lg-0">
                    <div class="text-center">
                        <img src="undraw_male_avatar_323b.png" height="150px;" width="150px;" >
                    <h3 class="mb-5" >Patient Details</h3>
                    </div>
                    <form action="apt.php" method="post">
                        <div class="form-group">
                            <label>Name:</label>
                            <input type="text" class="form-control" placeholder="Enter Name" name="name" value="<?php echo $_SESSION['patients']['name']; ?>" required >
                        </div>
                        <div class="form-group">
                            <input type="hidden" class="form-control" placeholder="Enter Name" name="id" value="<?php echo $_SESSION['patients']['id']; ?>" required >
                        </div>
                        <div class="form-group">
                            <label>Phone number:</label>
                            <input type="text" placeholder="Enter Phone" class="form-control" value="<?php echo $_SESSION['patients']['phoneno']; ?>" required name="phoneno">
                        </div>
                        <div class="form-group row">
                        <div class="col-sm-6 mb-3 mb-sm-0">
                            <label>Hospital name:</label>
                            <select class="form-control" name="hospital">
                                <option>--Select hospital--</option>
                                <option>Appllo</option>
                                <option></option>
                                <option></option>
                            </select>
                        </div>
                        <div class="col-sm-6">
                            <label>Slot timing:</label>
                            <select class="form-control" name="slot">
                                <option>--Select Slot--</option>
                                <option>Morning Slot</option>
                                <option>Evening Slot</option>
                            </select>
                        </div>
                        </div>
                        <div class="form-group">
                            <label>Date:</label>
                            <input type="date" name="date" placeholder="Date" class="form-control" required >
                        </div>
                        <div class="form-group">
                            <label>Your problem:</label>
                            <textarea  cols="20" rows="7"  placeholder="Message" class="form-control" required name="problem"></textarea>
                        </div>
                        <div class="text-center">
                          <button type="submit" name="aptbtn" class="btn btn-primary">Confirm appointment</button>
                        </div>
                        <br>
                    </form>
                </div>
            </div>
        </div>
          <div class="col-2">
          </div>
          <div class="col-4">

            <div class="card">
              <h3 class="text-center">Your Appointments</h3>
              <div class="col-lg-12 col-md-12 col-sm-12"><br>
                Patient Name:<?php echo $_SESSION['patients']['name']; ?><br>
                Hospital Name:<?php echo $_SESSION['patients']['hospital']; ?><br>
                Date:<?php echo $_SESSION['patients']['date']; ?><br>
                Slot:<?php echo $_SESSION['patients']['slot']; ?>
              </div>
            </div>
          </div>
        </div>
    </div>
</section>

  </body>
</html>
