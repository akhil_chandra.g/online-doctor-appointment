<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Receptionist</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<style media="screen">
li
{
  list-style: none;
}
.card{
  margin-top:100px;
}
  .container
  {
    margin-top: 40px;
    background-color: #3e64ff;
  }
</style>
  </head>
  <body>
    <div class="container">

    <div class="row" style="margin-top:50px;">
      <div class="col-lg-2 col-md-2 col-sm-2">
      </div>
      <div class="col-lg-6 col-md-6 col-sm-6 ">
          <h2 class="text-center">Appointments</h2>
      </div>
          <div class="col-lg-3 col-md-3">
          </div>
      </div>

      <div class="modal fade" id="deletemodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel"> Delete Category Data </h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
                    <form action="deletecode.php" method="POST">
                        <div class="modal-body">
                            <input type="hidden" name="delete_id" id="delete_id">
                            <h4> Do you want to Delete this Data ??</h4>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">  NO </button>
                            <button type="submit" name="deletedata" class="btn btn-primary"> Yes !! Delete it. </button>
                        </div>
                    </form>
                </div>
              </div>
            </div>
    <?php
        $connection = mysqli_connect("localhost","root","");
        $db = mysqli_select_db($connection, "online");

        $query = "SELECT * FROM appointments ";
        $query_run = mysqli_query($connection, $query);
    ?>
    <div class="row">

        <?php
          if($query_run)
          {
              foreach($query_run as $row)
              {
        ?>
                    <div class="col-lg-3 col-md-3 col-sm-3">
                      <div class="card shadow p-3 mb-5 bg-white rounded" style="margin-left:10px;">
                        <li> Patient Name: <span> <?php echo $row['name']; ?></span></li>
                        <li>Phonenumber:<span> <?php echo $row['phoneno']; ?></span></li>
                        <li>Slot Date:<span><?php echo $row['date']; ?></span> </li>
                        <li class="badge badge-primary">Slot timming:<?php echo $row['slot']; ?></li>
                        <div class="row" style="margin-top:8px">
                          <div class="col-lg-12 col-md-8 col-sm-8 text-center">
                            <button type="button" class="btn btn-primary" name="deletebtn" style="margin-left:100px;">
                                Accept</button>
                          </div>
                        </div>
                        </div>
                        </div>

        <?php
              }
          }
          else
          {
              echo "No Record Found";
          }
        ?>
</div>



    </div>
  </body>
</html>
