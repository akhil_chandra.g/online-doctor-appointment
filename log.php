<?php
	session_start();
	$db = mysqli_connect('localhost', 'root', '', 'online');
	$errors   = array();
	if (isset($_POST['login'])) {
		login();
	}

	if (isset($_GET['logout'])) {
		session_destroy();
		unset($_SESSION['patients']);
		header("location: login.php");
	}



	function getUserById($id){
		global $db;
		$query = "SELECT * FROM patients natural join appointments WHERE id=" .$id;
		$result = mysqli_query($db, $query);

		$patients = mysqli_fetch_assoc($result);
		return $patients;
	}

	function login(){
		global $db, $username, $errors;


		$username = e($_POST['username']);
		$password = e($_POST['password']);


		if (empty($username)) {
			array_push($errors, "Username is required");
		}
		if (empty($password)) {
			array_push($errors, "Password is required");
		}


		if (count($errors) == 0) {
			$password = md5($password);

			$query = "SELECT * FROM patients natural join appointments  WHERE username='$username' AND password='$password' LIMIT 1";
			$results = mysqli_query($db, $query);

			if (mysqli_num_rows($results) == 1) {
				$logged_in_user = mysqli_fetch_assoc($results);

					$_SESSION['patients'] = $logged_in_user;
					$_SESSION['success']  = "You are now logged in";
					header('location: patient.php');
				}
		else {
				array_push($errors, "Wrong username/password combination");
			}
		}
	}

	function isLoggedIn()
	{
		if (isset($_SESSION['patients'])) {
			return true;
		}else{
			return false;
		}
	}

	function e($val){
		global $db;
		return mysqli_real_escape_string($db, trim($val));
	}

	function display_error() {
		global $errors;

		if (count($errors) > 0){
			echo '<div class="error">';
				foreach ($errors as $error){
					echo $error .'<br>';
				}
			echo '</div>';
		}
	}

?>
